var gulp       = require('gulp'), // Подключаем Gulp
    sass         = require('gulp-sass'), //Подключаем Sass пакет,
    browserSync  = require('browser-sync'), // Подключаем Browser Sync
    concat       = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    // uglify       = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
    cssnano      = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename       = require('gulp-rename'), // Подключаем библиотеку для переименования файлов
    del          = require('del'), // Подключаем библиотеку для удаления файлов и папок
    imagemin     = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant     = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache        = require('gulp-cache'), // Подключаем библиотеку кеширования
    pug = require('gulp-pug');
    autoprefixer = require('gulp-autoprefixer');// Подключаем библиотеку для автоматического добавления префиксов

gulp.task('pug', function() {
    return gulp.src("src/pug/*.pug")
        .pipe(pug())
        .pipe(gulp.dest("dist/pug"))
        .pipe(browserSync.stream());
});
gulp.task('sass', function(){ // Создаем таск Sass
    return gulp.src('src/sass/**/*.scss') // Берем источник
        .pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
        .pipe(gulp.dest('src/css')) // Выгружаем результата в папку app/css
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});

gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browserSync
        server: { // Определяем параметры сервера
            baseDir: 'src' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});
//begining
gulp.task('scripts', function() {
    return gulp.src([ // Берем все необходимые библиотеки
        'app/libs/jquery/dist/jquery.min.js', // Берем jQuery
        'app/libs/magnific-popup/dist/jquery.magnific-popup.min.js' // Берем Magnific Popup
    ])
        .pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(uglify()) // Сжимаем JS файл
        .pipe(gulp.dest('app/js')); // Выгружаем в папку app/js
});
//end js file
gulp.task('css-libs', ['sass'], function() {
    return gulp.src('src/css/libs.css') // Выбираем файл для минификации
        .pipe(cssnano()) // Сжимаем
        .pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
        .pipe(gulp.dest('src/css')); // Выгружаем в папку app/css
});

gulp.task('watch', ['browser-sync', 'css-libs'], function() {   //, 'scripts'
    gulp.watch('src/sass/**/*.scss', ['sass']); // Наблюдение за sass файлами в папке sass
    gulp.watch('src/sass/components/*.scss', ['sass']); // Наблюдение за sass файлами в папке sass
    gulp.watch('src/*.html', browserSync.reload); // Наблюдение за HTML файлами в корне проекта
    gulp.watch('src/pug/*.pug', browserSync.reload); // Наблюдение за PUG файлами в корне проекта
    // gulp.watch('src/js/**/*.js', browserSync.reload);   // Наблюдение за JS файлами в папке js
});

gulp.task('clean', function() {
    return del.sync('dist'); // Удаляем папку dist перед сборкой
});

gulp.task('img', function() {
    return gulp.src('src/img/**/*') // Берем все изображения из app
        .pipe(cache(imagemin({  // Сжимаем их с наилучшими настройками с учетом кеширования
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img')); // Выгружаем на продакшен
});

gulp.task('build', ['clean', 'img', 'pug','sass'], function() { //,  'img', 'scripts'

    var buildCss = gulp.src([ // Переносим библиотеки в продакшен
        'src/css/main.css',
        // 'src/css/libs.min.css'
    ])
        .pipe(gulp.dest('dist/css'));

    var buildFonts = gulp.src('src/fonts/**/*') // Переносим шрифты в продакшен
        .pipe(gulp.dest('dist/fonts'));

    // var buildJs = gulp.src('app/js/**/*') // Переносим скрипты в продакшен
    //     .pipe(gulp.dest('dist/js'))

    var buildHtml = gulp.src('src/*.html') // Переносим HTML в продакшен
        .pipe(gulp.dest('dist'));

});
gulp.task('clear', function () {
    return cache.clearAll();
})
gulp.task('default', ['watch', 'build']);




// let gulp = require('gulp');
// let browserSync = require('browser-sync').create();
// let sass = require('gulp-sass');
// let autoprefixer = require('gulp-autoprefixer');
// let concatCss = require('gulp-concat-css');
// let cleanCSS = require('gulp-clean-css');
// let rename = require("gulp-rename");
// let uglify = require('gulp-uglify');
// let pug = require('gulp-pug');
// let del = require('del'); // Подключаем библиотеку для удаления файлов и папок
// //Html from src to dist
// gulp.task('clean', function() {
//     return del.sync('dist'); // Удаляем папку dist перед сборкой
// });
// gulp.task('build', ['clean','sass'], function() {
//     var buildHtml = gulp.src('src/html/*.html') // Переносим HTML в продакшен
//         .pipe(gulp.dest('dist'));
// });
// // Compile sass into CSS & auto-inject into browsers
// gulp.task('sass', function () {
//     return gulp.src("src/sass/*.scss")
//         .pipe(sass())
//         .pipe(gulp.dest("dist/css"))
//         .pipe(browserSync.stream());
// });
// // Static Server + watching scss/html files
// gulp.task('serve', ['sass'], function () {
//     browserSync.init({
//         server: "dist" + "",
//         open: true,
//         notify: false
//     });
//     gulp.watch("src/sass/*.scss", ['sass']);
//     gulp.watch("src/html/*.html").on('change', browserSync.reload);
// });
// // ---Main task---
// gulp.task('default', ['build', 'sass', 'serve']);
//
//
// // gulp.task('pug', function () {
// //     return gulp.src('src/pug/*.pug')
// //         .pipe(pug())
// //         .pipe(gulp.dest('dist/html'))
// // });
//
//
// //
// // gulp.task('sass', function () {
// //     return gulp.src('./sass/**/*.scss')
// //         .pipe(sass().on('error', sass.logError))
// //         .pipe(gulp.dest('dist/css'));
// // });
// //
// // gulp.task('sass:watch', function () {
// //     gulp.watch('./sass/**/*.scss', ['sass']);
// // });
// // gulp.task('pug', function () {
// //     return gulp.src('src/pug/*.pug')
// //         .pipe(pug())
// //         .pipe(gulp.dest('www'))
// // });
// //
// // gulp.task('watch',function () {
// //     gulp.watch('src/pug/*', ['pug']);
// // });
// //
// // gulp.task('default', ['pug','watch']);
// // var buildCss = gulp.src([ // Переносим CSS стили в продакшен
// //     'app/css/main.css',
// //     'app/css/libs.min.css'
// // ])

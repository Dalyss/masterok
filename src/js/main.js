function initMap() {
    var pos = {lat: 40.7143528, lng: -74.0059731};

    geocoder = new google.maps.Geocoder();
    infowindow = new google.maps.InfoWindow;
    map = new google.maps.Map(document.getElementById('map'), {
        center: pos,
        // disableDefaultUI: true,
        zoom: 8,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#856363"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ],
        mapTypeControl: false,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        scaleControl: false,
        streetViewControl: false,

        fullscreenControl: true,
        fullscreenControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        }
    });
    marker = new google.maps.Marker({
        map: map,
        position: pos,
        title: ''
    });


    // if (navigator.geolocation) {
    //     navigator.geolocation.getCurrentPosition(
    //         function (position) {
    //             findGeolocation(position)
    //         }, function () {
    //             handleLocationError(true, infowindow, map.getCenter());
    //         });
    // } else {
    //     handleLocationError(false, infowindow, map.getCenter());
    // }

    google.maps.event.addListener(map, 'click', function () {
        infowindow.close();
    });
}